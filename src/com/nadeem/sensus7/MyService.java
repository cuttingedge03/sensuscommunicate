package com.nadeem.sensus7;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.CursorJoiner.Result;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.widget.Toast;

public class MyService extends Service implements SensorEventListener {
	WifiManager wifi;
	List<ScanResult> results;
	boolean stopFlag = false;
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	float x, y, z;
	Socket socket;
	List<Sensor> list;
	String ip;
	int port, i;
	boolean flag;
	WifiInfo wifiInfo;

	// Handler scanHandler = new Handler ();
	// Runnable UpdateUIResults = new Runnable () {
	// public void run () {
	// updateUI ();
	// }
	//
	// public void updateUI() {
	// MainActivity.parent.removeAllViews();
	// TextView tv;
	// for (int i = 0; i < results.size(); i++) {
	// tv = new TextView(getApplicationContext());
	// tv.setText("SSID is "+results.get(i).SSID+" with strength: "+results.get(i).level);
	// tv.setTextColor(Color.BLACK);
	// MainActivity.parent.addView(tv);
	// //System.out.println("SSID is "+results.get(i).SSID+" with strength: "+results.get(i).level);
	// }
	// }
	// };

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// ip = intent.getExtras().getString("ip");
		// port = intent.getExtras().getInt("port");

		return super.onStartCommand(intent, flags, startId);

	}

	public MyService() {
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO: Return the communication channel to the service.
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public void onCreate() {
		wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		wifiInfo = wifi.getConnectionInfo();
		mSensorManager = (SensorManager) this
				.getSystemService(Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mSensorManager.registerListener((SensorEventListener) this,
				mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

		list = mSensorManager.getSensorList(Sensor.TYPE_ALL);
		flag = true;
		wifi.startScan();
		results = wifi.getScanResults();
		System.out.println(results);
		for (int i = 1; i < 5; i++) {
			startMyTask(new MyAsyncTask(i));
		}

	}

	@Override
	public void onStart(Intent intent, int startId) {
	}

	@Override
	public void onDestroy() {
		flag = false;
		mSensorManager.unregisterListener(this);

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			x = event.values[0];
			y = event.values[1];
			z = event.values[2];

		}

	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	public void toast(String text) {
		Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT)
				.show();
	}

	private class MyAsyncTask extends AsyncTask<Void, Void, Boolean> {

		int counter;
		int id;

		public MyAsyncTask(int id) {
			this.id = id;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub

			while (true) {
				System.out.println("counter" + id + " = " + counter);
				wifi.startScan();
				results = wifi.getScanResults();
				try {

					socket = new Socket("192.168.0.104", 8888);
					System.out.println("socket established!");
					// ++i;

					// BufferedReader fromServer = null;
					PrintWriter toServer = null;
					// if (i > 0) {

					// }
					// }

					// fromServer = new BufferedReader(new InputStreamReader(
					// socket.getInputStream()));
					toServer = new PrintWriter(socket.getOutputStream(), true);
					String str = new String();

					str = x + "," + y + "," + z + "," + results.get(0).SSID
							+ "," + String.valueOf(results.get(0).level) + ","
							+ results.get(0).frequency + ","
							+ results.get(0).timestamp;

					// toServer.println(x);
					// toServer.println(y);
					// toServer.println(z);
					toServer.println(str);
					System.out.println(str);
					System.out.println("sent");

					// from.append(fromServer.readLine() + "\n");
					// from.append(fromServer.readLine() + "\n");
					// from.append(fromServer.readLine() + "\n");
					// from.append(fromServer.readLine());
					// System.out.println(fromServer.readLine());
					// System.out.println("received");
					toServer.flush();
					// socket.close();
					// fromServer.close();

				} catch (Exception e) {
					e.printStackTrace();
				}
				// System.out.println(send());

				counter += 1;
				if (flag == false) {

					break;
				}

			}

			return false;
		}

		protected void onPostExecute(Boolean a) {
			System.out.println("end");

		}
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	// API 11
	void startMyTask(AsyncTask<Void, Void, Boolean> asyncTask) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
			asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		else
			asyncTask.execute();
	}
}
