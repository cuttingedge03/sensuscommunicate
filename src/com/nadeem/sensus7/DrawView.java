package com.nadeem.sensus7;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.view.View;

public class DrawView extends View {

	Paint paint = new Paint();
	Path wallpath = new Path();
	Context context;
	Bitmap bmp;
	List<String> list;

	public DrawView(Context context, List<String> list) {
		super(context);
		this.context = context;
		this.list = list;
		// setLayerType(View.LAYER_TYPE_HARDWARE, null);

	}
	public DrawView(Context context) {
		super(context);
		// setLayerType(View.LAYER_TYPE_HARDWARE, null);

	}

	@Override
	public void onDraw(Canvas canvas) {

		paint.setColor(Color.BLACK);
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(10);

		for (int i = 0; i < list.size(); i = i + 2) {
			if (i == 0)
				wallpath.moveTo((Float.parseFloat(list.get(i))),
						(Float.parseFloat(list.get(i + 1))));
			else
				wallpath.lineTo(Float.parseFloat(list.get(i)),
						(Float.parseFloat(list.get(i + 1))));
			canvas.drawPath(wallpath, paint);
		}

	}
}
