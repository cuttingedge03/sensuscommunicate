package com.nadeem.sensus7;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	RelativeLayout rl;
	private final HttpClient httpclient = new DefaultHttpClient();

	public int SOURCE = 0;
	public int DESTINATION = 1;
	final HttpParams params = httpclient.getParams();
	HttpResponse response;
	public float x, y;
	public String sWidth, sHeight, sX, sY, dX, dY;
	public String SCREEN_HEIGHT = "SCREEN_HEIGHT";
	public String SCREEN_WIDTH = "SCREEN_WIDTH";
	public String SOURCEX = "SOURCEX";
	public String SOURCEY = "SOURCEY";
	public String DESTINATIONX = "DESTINATIONX";
	public String DESTINATIONY = "DESTINATIONY";
	RelativeLayout.LayoutParams param;
	ImageView im;

	private Bitmap bmp;

	private int[][] rgbValues;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		rl = (RelativeLayout) findViewById(R.id.container);
		Toast.makeText(
				getApplicationContext(),
				"Available cores "
						+ Runtime.getRuntime().availableProcessors(),
				Toast.LENGTH_SHORT).show();
		// im = (ImageView) findViewById(R.id.image);

		// rl.getViewTreeObserver().addOnGlobalLayoutListener(
		// new OnGlobalLayoutListener() {
		// public void onGlobalLayout() {
		// rl.getViewTreeObserver().removeGlobalOnLayoutListener(
		// this);
		//
		// final Bitmap bmp = ((BitmapDrawable) im.getDrawable())
		// .getBitmap();
		// rgbValues = new int[40][19];
		// int[] pixels = new int[40];
		// int flag = 0, l = 0;
		// for (int i = 389; i < 429; i++) {
		// for (int j = 657; j <676; j++) {
		// flag = 1;
		// rgbValues[i-389][j-657] = bmp.getPixel(i, j);
		// for (int k = 0; k < pixels.length; k++) {
		// if (rgbValues[i-389][j-657] == pixels[k])
		// flag = 0;
		// }
		// if (flag == 1) {
		// pixels[l] = rgbValues[i-389][j-657];
		// System.out.println(pixels[l] + " "
		// + Color.alpha(pixels[l]) + " "
		// + Color.red(pixels[l]) + " "
		// + Color.green(pixels[l]) + " "
		// + Color.blue(pixels[l]));
		// l++;
		// }
		//
		// }
		//
		// }
		// }
		// });

		param = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT);

		rl.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				rl.removeAllViews();
				x = event.getX();
				y = event.getY();
				// float[] eventXY = new float[] { x, y };
				//
				// Matrix invertMatrix = new Matrix();
				// ((ImageView) v).getImageMatrix().invert(invertMatrix);
				//
				// invertMatrix.mapPoints(eventXY);
				// int pX = Integer.valueOf((int) eventXY[0]);
				// int pY = Integer.valueOf((int) eventXY[1]);
				// Drawable imgDrawable = ((ImageView)v).getDrawable();
				// Bitmap bitmap = ((BitmapDrawable)imgDrawable).getBitmap();
				// // final Bitmap bitmap = ((BitmapDrawable) im.getDrawable())
				// // .getBitmap();
				// if (x < 0) {
				// x = 0;
				// } else if (x > bitmap.getWidth() - 1) {
				// x = bitmap.getWidth() - 1;
				// }
				//
				// if (y < 0) {
				// y = 0;
				// } else if (y > bitmap.getHeight() - 1) {
				// y = bitmap.getHeight() - 1;
				// }
				//
				// int pixel = bitmap.getPixel(pX, pY);
				//
				// // int redValue = Color.red(pixel);
				// // int blueValue = Color.blue(pixel);
				// // int greenValue = Color.green(pixel);
				// System.out.println(pixel + " "
				// + Color.alpha(pixel) + " "
				// + Color.red(pixel) + " "
				// + Color.green(pixel) + " "
				// + Color.blue(pixel));

				sWidth = String.valueOf(getScreenWidth());
				sHeight = String.valueOf(getScreenHeight());
				getSelection(x, y);
				System.out.println("touched");
				System.out.println(x + "and" + y);
				// drawPath(x, y);
				return false;
			}
		});
	}

	protected void getSelection(final float x, final float y) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Options...");
		builder.setItems(R.array.curve_array,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (which == SOURCE) {
							sX = String.valueOf(x);
							sY = String.valueOf(y);
						} else if (which == DESTINATION) {
							dX = String.valueOf(x);
							dY = String.valueOf(y);
							startService(new Intent(getApplicationContext(),
									MyService.class));
							new MyAsyncTask().execute();
						}
					}

				});
		AlertDialog alert = builder.create();
		alert.show();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.stop) {
			stopService(new Intent(this, MyService.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public int getScreenHeight() {
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			display.getSize(size);
			return (size.y);
		} else {
			return (display.getHeight());
		}
	}

	public int getScreenWidth() {
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			display.getSize(size);
			return (size.x);
		} else {
			return (display.getWidth());
		}
	}

	private class MyAsyncTask extends AsyncTask<Void, Void, List<String>> {

		private Socket socket;

		@Override
		protected List<String> doInBackground(Void... params) {
			// TODO Auto-generated method stub

			return postData();
		}

		protected void onPostExecute(List<String> result) {

			// try {
			//
			// JSONArray jsonArray = new JSONArray(result);
			// List<String> list = new ArrayList<String>();
			//
			// for (int i = 0; i < jsonArray.length(); i++) {
			// list.add(jsonArray.getString(i));
			// }

			drawPath(result);
			// } catch (JSONException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

		}

		public List<String> postData() {
			// String origresponseText = "";

			// List<NameValuePair> nameValuePairs = new
			// ArrayList<NameValuePair>();
			// nameValuePairs.add(new BasicNameValuePair(SOURCEX, sX));
			// nameValuePairs.add(new BasicNameValuePair(SOURCEY, sY));
			// nameValuePairs.add(new BasicNameValuePair(DESTINATIONX, dX));
			// nameValuePairs.add(new BasicNameValuePair(DESTINATIONY, dY));
			// nameValuePairs.add(new BasicNameValuePair(SCREEN_HEIGHT,
			// sHeight));
			// nameValuePairs.add(new BasicNameValuePair(SCREEN_WIDTH, sWidth));
			ArrayList<String> fromServerList = new ArrayList<String>();
			List<String> values = new ArrayList<String>();
			values.add(sX);
			values.add(sY);
			values.add(dX);
			values.add(dY);
			values.add(sHeight);
			values.add(sWidth);
			try {
				socket = new Socket("192.168.0.106", 4444);
				System.out.println("socket established!");
				ObjectOutputStream objectOutput = new ObjectOutputStream(
						socket.getOutputStream());
				objectOutput.writeObject(values);
				ObjectInputStream objectInput = new ObjectInputStream(
						socket.getInputStream());
				Object object = objectInput.readObject();
				fromServerList = (ArrayList<String>) object;

				//
				// HttpClient client = new DefaultHttpClient();
				// String postURL = "http://192.168.43.24:8080/Pro1/Serv1";
				// HttpPost post = new HttpPost(postURL);
				// UrlEncodedFormEntity ent = new UrlEncodedFormEntity(
				// nameValuePairs, HTTP.UTF_8);
				// post.setEntity(ent);
				// HttpResponse responsePOST = client.execute(post);
				// HttpEntity resEntity = responsePOST.getEntity();
				// origresponseText = EntityUtils.toString(resEntity);
				// if (resEntity != null) {
				// // Log.i("RESPONSE", EntityUtils.toString(resEntity));
				// }
			} catch (Exception e) {
				e.printStackTrace();
			}
			// origresponseText = readContent(response);
			// return origresponseText;
			return fromServerList;
		}
	}

	public void drawPath(List<String> list) {
		rl.postInvalidate();
		DrawView d = new DrawView(this, list);
		d.setLayoutParams(param);
		rl.addView(d, param);
		// ImageView i = new ImageView(this);
		// i.setBackgroundResource(R.drawable.ic_action_place);
		// param = new RelativeLayout.LayoutParams(
		// RelativeLayout.LayoutParams.WRAP_CONTENT,
		// RelativeLayout.LayoutParams.WRAP_CONTENT);
		// param.leftMargin = Integer.parseInt(list.get(0));
		// param.topMargin = Integer.parseInt(list.get(1));
		// rl.addView(i, param);
		// ImageView iv = new ImageView(this);
		// iv.setBackgroundResource(R.drawable.ic_action_place);
		// param = new RelativeLayout.LayoutParams(
		// RelativeLayout.LayoutParams.WRAP_CONTENT,
		// RelativeLayout.LayoutParams.WRAP_CONTENT);
		// param.leftMargin = Integer.parseInt(list.get(list.size() - 2));
		// param.topMargin = Integer.parseInt(list.get(list.size() - 1));
		// rl.addView(iv, param);
	}

	// public void drawPath(float x, float y) {
	// rl.postInvalidate();
	// DrawViews d = new DrawViews(this);
	// d.setLayoutParams(param);
	// rl.addView(d, param);
	//
	// }
	//
	// public class DrawViews extends View {
	//
	// Paint paint = new Paint();
	//
	// public DrawViews(Context context) {
	// super(context);
	//
	// }
	//
	// @Override
	// public void onDraw(Canvas canvas) {
	//
	// paint.setColor(Color.BLACK);
	// paint.setStyle(Style.FILL_AND_STROKE);
	// paint.setStrokeWidth(15);
	// canvas.drawPoint(x, y, paint);
	//
	// }
	// }

}
